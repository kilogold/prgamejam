# README #

### What is this repository for? ###

PR Game Jam VRTS
This is a mobile Real-Time Strategy for VR.  
Currently, it supports cardboard, yet is originally intended to work with GearVR.  
Due to Game Jam limitations, only Cardboard was developed.  
Final deliverable will be deployed for both Cardboard and GearVR.  

### How do I get set up? ###

* Obtain Android device able to run Cardboard.  
* Build and run to android device using Unity3D 5.1.2 or later.  

### Who do I talk to? ###

* kelvin.bonilla@kraniumtivity.com
* anner.bonilla@kraniumtivity.com