using UnityEngine;
using System.Collections;

public class AiState_TankIdle : AiStateBase
{
    public override void OnExit()
    {
    }

    public override void OnEnter()
    {
        GetComponent<NavMeshAgent>().enabled = false;
    }
}
