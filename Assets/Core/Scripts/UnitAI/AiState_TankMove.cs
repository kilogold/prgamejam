using UnityEngine;
using System.Collections;

public class AiState_TankMove : AiStateBase
{
    private NavMeshAgent navAgent;
    void Awake()
    {
        // This crap doesn't get called for some reason.
        // Fuck it, do it on OnEnable.
        navAgent = GetComponent<NavMeshAgent>();
    }

    public override void OnExit()
    {
        navAgent.enabled = false;
    }

    public override void OnEnter()
    {
        navAgent = GetComponent<NavMeshAgent>();
        navAgent.enabled = true;
    }

    public void SetDestination( Vector3 point)
    {
        navAgent.destination = point;
    }

    void Update()
    {
        float dist = navAgent.remainingDistance;
        if ( dist != Mathf.Infinity &&
            navAgent.pathStatus == NavMeshPathStatus.PathComplete &&
            dist == 0 )
        {
            stateMachine.SetState(GetComponent<AiState_TankIdle>());
        }
        /*GameObject[] nearbyUnits = GameObject.FindGameObjectsWithTag("AggroSphere");
        foreach(GameObject unit in nearbyUnits )
        {
            dist =  Vector3.Distance(this.transform.position,unit.transform.position);
            float rad = this.GetComponentInChildren<AggroSphereCollision>().GetComponent<SphereCollider>().radius;
            if(dist <= rad)
            {
                //another enemy is inside
                //check taht is not myself

            }
        }*/
        //check if we have another enemy unit nearby
        //check if we touched the sphere of a enemy building or unit

   } 
}
