using UnityEngine;
using System.Collections;

public class AiStateMachine : MonoBehaviour
{

    public AiStateBase[] allStates;
    public AiStateBase currentState;

    void Awake()
    {
        allStates = GetComponents<AiStateBase>();
    }

    public void SetState( AiStateBase newState )
    {
        if (null == newState)
        {
            Debug.LogError("Passing in null state. Ignoring");
            return;
        }        
        
        if (currentState == newState)
            return;

        foreach (AiStateBase aiState in allStates)
        {
            aiState.OnExit();
            aiState.enabled = false;
        }

        currentState = newState;

        currentState.OnEnter();
        currentState.enabled = true;
    }
}
