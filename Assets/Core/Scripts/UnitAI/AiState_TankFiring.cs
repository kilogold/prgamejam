using UnityEngine;
using System.Collections;

public class AiState_TankFiring : AiStateBase
{
    public GameObject TargetUnit;
    // Use this for initialization

    public override void OnEnter()
    {
        StartCoroutine(DoDamage());
    }
    public override void OnExit()
    {
        StopAllCoroutines();
    }
    // Update is called once per frame
    IEnumerator DoDamage() 
    {
        while(true)
        {
            //no unit to shoot at
            UnitStatsBehavior stats = GetComponent<UnitStatsBehavior>();
            if(null == TargetUnit )
            {
                stateMachine.SetState(GetComponent<AiState_TankIdle>());
                break;
            }
            AudioSource[] audio = GetComponentsInParent<AudioSource>();
            if(TargetUnit.CompareTag("AggroSphere"))
            {
                //apply damage to the unit we found
                //call the commander
               
                UnitStatsBehavior enemyStats = TargetUnit.GetComponentInParent<UnitStatsBehavior>();
                if (null == enemyStats)
                {
                    Debug.Log("enemy unit has no stats");
                    break;
                }
                enemyStats.ApplyDamage(stats.baseStats.damage);
                if (enemyStats.currentHealth <= 0)
                {
                    enemyStats.GetComponent<AiStateMachine>().SetState(enemyStats.GetComponent<AiState_TankDead>());
                    if (null != audio[1])
                    {
                        audio[1].Play();
                    }
                    break;
                }
                //play sound?
                
                if (null != audio[0])
                {
                    audio[0].Play();
                }
                yield return new WaitForSeconds(stats.baseStats.shootingCooldown);
            }
            else if (TargetUnit.CompareTag("Building"))
            {

                UnitFactory enemyStats = TargetUnit.GetComponent<UnitFactory>();
                if (null == enemyStats)
                {
                    Debug.Log("enemy building has no unitfactory");
                    break;
                }
                enemyStats.ApplyDamage(stats.baseStats.damage);
                if (enemyStats.currentHealth <= 0)
                {
                    stats.GetComponent<AiStateMachine>().SetState(stats.GetComponent<AiState_TankIdle>());

                    //This tank won the battle. Notify its commander of Mission Complete.
                    // Commander will inform all other commanders that he is the victor.
                    stats.CommanderOwnership.Commander.FlagCommanderAsVictor();
                    Debug.Log("<color=yellow>factory destroyed</color>");
                    if (null != audio[1])
                    {
                        audio[1].Play();
                    }
                    break;
                }
                //play sound?

                if (null != audio[0])
                {
                    Debug.Log("Cannot find audio source");
                    audio[0].Play();
                }
                yield return new WaitForSeconds(stats.baseStats.shootingCooldown);
            }

           

            

        }
        stateMachine.SetState(GetComponent<AiState_TankIdle>());
	}
}
