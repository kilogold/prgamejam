using UnityEngine;
using System.Collections;

public class AiState_TankDead : AiStateBase
{
    public override void OnExit()
    {
    }

    public override void OnEnter()
    {
        gameObject.SetActive(false);
    }
}
