using UnityEngine;
using System.Collections;

public abstract class AiStateBase : MonoBehaviour
{
    protected AiStateMachine stateMachine;
    
    void Start()
    {
       stateMachine = GetComponent<AiStateMachine>();
    }

    public abstract void OnEnter();
    public abstract void OnExit();
}
