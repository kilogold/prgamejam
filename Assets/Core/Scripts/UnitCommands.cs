using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class UnitCommands : NetworkBehaviour
{
    private AiStateMachine stateMachine;

    void Awake()
    {
        stateMachine = GetComponent<AiStateMachine>();
    }

    public void Attack( GameObject target)
    {
        
    }

    public void Move(Vector3 target)
    {
        AiState_TankMove moveState = GetComponent<AiState_TankMove>();

        stateMachine.SetState( moveState );

        moveState.SetDestination(target);
    }
}
