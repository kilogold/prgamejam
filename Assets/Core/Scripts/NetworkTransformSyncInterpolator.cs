using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkTransformSyncInterpolator : NetworkBehaviour
{
    [SyncVar] public Vector3 syncPosition;
    [SyncVar] public Quaternion syncQuaternion;

    private UnitStatsBehavior unitStatsBehaviour;

    // Use this for initialization
    void Start()
    {
        unitStatsBehaviour = GetComponent<UnitStatsBehavior>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        TransmitPosition();
        LerpPosition();
    }

    void LerpPosition()
    {
        if (false == isLocalPlayer)
        {
            transform.position = Vector3.Lerp(transform.position, syncPosition, Time.deltaTime * unitStatsBehaviour.unitSpeed);
            transform.rotation = syncQuaternion;
        }
    }

    [ClientCallback]
    void TransmitPosition()
    {
        if (isLocalPlayer)
        {
            CmdSendPositionToServer(transform.position, transform.rotation);
        }
    }

    [Command]
    void CmdSendPositionToServer( Vector3 pos, Quaternion quat)
    {
        syncPosition = pos;
        syncQuaternion = quat;
    }
}
