using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class UnitFactory : NetworkBehaviour
{
    public int maxHealth;
    public int currentHealth;
    public GameObject agroRadiusPrefab;
    public GameObject unitPrefab;
    public Transform spawnPoint;
    public Transform rallyPoint;
    public NetworkCommanderOwnership CommanderOwnership { get; private set; }

    void Awake()
    {
        currentHealth = maxHealth = 200;
        CommanderOwnership = GetComponent<NetworkCommanderOwnership>();
    }

    /// <summary>
    /// Set the color of the factory based on commander.
    /// </summary>
    [ClientRpc]
    private void RpcSetTeamColor( Color commanderMaterialColor )
    {
        GetComponent<Renderer>().material.color = commanderMaterialColor;
    }
    public void ApplyDamage(int damage)
    {
        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            gameObject.SetActive(false);

            //Factory is now dead.
            //Our commander will tell all other commanders that he lost by
            //anouncing "player deafeated" and sending the id of that player.

            //Recipients of this message need only check their playerID with the incoming one.
            //If it matches, present losing toast. If no match, present winning toast.

            Debug.Log("<color=lime>factory destroyed</color>");
        }
    }
    //This method should ONLY be called by a NetworkCommander.
    //I can't think of a design pattern to limit this to only that class.
    //...It's 2am...
    public void AssignCommander(NetworkCommander commanderIn)
    {
        CommanderOwnership.AssignCommander(commanderIn);
        RpcSetTeamColor(CommanderOwnership.Commander.OwnershipMaterial.color);
    }

    public void SpawnUnitForCommander()
    {
        // We can't spawn something from a factory that is not owned by anyone.
        if (CommanderOwnership.commanderNetId.IsEmpty())
        {
            Debug.LogException(new NullReferenceException("No commander assigned to UnitFactory. ObjID: " + gameObject.GetInstanceID()));
        }

        // Create and spawn the new unit.
        GameObject newUnit = Instantiate(unitPrefab, spawnPoint.position, Quaternion.identity) as GameObject;
        GameObject unitAgro = Instantiate(agroRadiusPrefab, Vector3.zero, Quaternion.identity) as GameObject;
        unitAgro.transform.parent = newUnit.transform;
        unitAgro.transform.localPosition = Vector3.zero;
        NetworkServer.Spawn(newUnit);
        newUnit.GetComponent<UnitStatsBehavior>().AssignCommander(CommanderOwnership.Commander);


        // Only host-side units will have pathfinding.
        // We must add pathfinding dynamically.
        // Since clients send the net Command to the host, this implicitly
        // means that this code is only run by the server ( you can't spawn from the 
        // client ).
        Debug.Log("Initializing LOCAL player for ObjectID: " + newUnit.gameObject.GetInstanceID());

        UnitStatsBehavior statsBehaviour = newUnit.GetComponent<UnitStatsBehavior>();
        NavMeshAgent agent = newUnit.gameObject.AddComponent<NavMeshAgent>();
        agent.speed = statsBehaviour.unitSpeed;
        agent.angularSpeed = statsBehaviour.unitRotationSpeed;
        agent.enabled = false; // We will let AI enable it when necessary.

        // We must add states for AI as well
        newUnit.AddComponent<AiState_TankMove>().enabled = false;
        newUnit.AddComponent<AiState_TankIdle>().enabled = false;
        newUnit.AddComponent<AiState_TankFiring>().enabled = false;
        newUnit.AddComponent<AiState_TankDead>().enabled = false;
        newUnit.AddComponent<AiStateMachine>();
        
        UnitCommands unitCommands = newUnit.AddComponent<UnitCommands>();
        unitCommands.Move(rallyPoint.position);
    }
}
