using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkCommanderInterface : MonoBehaviour
{
    public GameObject selectedUnit;
    private NetworkCommander commander;
    public Transform cameraHead;
    public bool IsOnCooldown;
    void Start()
    {
        IsOnCooldown = false;
        commander = GetComponent<NetworkCommander>();
    }
    IEnumerator  Cooldown()
    {
            IsOnCooldown = true;
            
            yield return new WaitForSeconds(3);

            IsOnCooldown = false;
    }
    void Update()
    {
        if( Input.GetButtonDown("Fire1") )
        {
            RaycastHit hit;
            if (Physics.Raycast(cameraHead.position, cameraHead.forward, out hit))
            {
                
                if (hit.transform.gameObject.CompareTag("Unit"))
                {
                    selectedUnit = hit.transform.gameObject;
                    Debug.Log("Found " + hit.transform.gameObject.name);

                }

                if (hit.transform.gameObject.CompareTag("Building"))
                {
                    Debug.Log("Found " + hit.transform.gameObject.name);

                    // Only operate the building if it is owned by us.
                    UnitFactory unitFactory = hit.transform.GetComponent<UnitFactory>();
                    Debug.Log("Comparing Owners: " + unitFactory.CommanderOwnership.commanderNetId + " vs " + commander.netId);
                    if (unitFactory.CommanderOwnership.commanderNetId == commander.netId)
                    {
                        if (!IsOnCooldown)
                        {
                            commander.CmdSpawnObjectFromFactory(hit.transform.gameObject);
                            StartCoroutine(Cooldown());
                        }
                        else
                        {
                            GetComponent<AudioSource>().Play();
                        }
                    }

                    // Own the building if not owned by anyone.
                    else if (unitFactory.CommanderOwnership.commanderNetId.IsEmpty())
                    {
                        
                            commander.CmdAssignCommanderToFactory(unitFactory.gameObject);
                        
                    }
                }
            }
        }
        else if (Input.GetButtonDown("Fire2"))
        {
            if (selectedUnit == null)
                return;

            RaycastHit hit;
            if (Physics.Raycast(cameraHead.position, cameraHead.forward, out hit))
            {
                if (hit.transform.gameObject.CompareTag("Terrain"))
                {
                    // Only operate the selected unit if it is owned by us.
                    UnitStatsBehavior unitStatsBehavior = selectedUnit.GetComponent<UnitStatsBehavior>();
                    if (unitStatsBehavior.CommanderOwnership.commanderNetId == commander.netId)
                    {

                        commander.CmdMoveUnit(selectedUnit, hit.point);
                    }
                }
            }
        }
    }
}
