using UnityEngine;
using System.Collections;

public class SceneViewSelectedIcon : MonoBehaviour
{
    public Transform target;
    public float radius;
    public Color color;

    public void OnDrawSelectedGizmos()
    {
        if (!target)
            return;

        Gizmos.color = color;
        Gizmos.DrawSphere(target.position, radius);
    }
}
