using UnityEngine;
using System.Collections;

//Should be scriptable object
public class UnitStats : ScriptableObject
{
    public int movementSpeed;
    public int rotationSpeed;
    public int maxHealth;
    public int damage;
    public float shootingCooldown;
}
