using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class AutoHost : MonoBehaviour
{

    // Use this for initialization
    private static  bool IsStarted = false;
    IEnumerator Start()
    {
        if(IsStarted)
        {
            yield break;
        }
        NetworkManager manager = GameObject.FindObjectOfType<NetworkManager>();
        manager.StartHost();

        yield return new WaitForSeconds(1.0f);
        if (IsStarted)
        {
            yield break;
        }
        ClientScene.Ready(manager.client.connection);
        IsStarted = true;
        if (ClientScene.localPlayers.Count == 0)
        {
            ClientScene.AddPlayer(0);
        }
    }
}
