using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class AutoJoin : MonoBehaviour
{
    public string ip;

    // Use this for initialization
    IEnumerator Start()
    {
        NetworkManager manager = GameObject.FindObjectOfType<NetworkManager>();
        manager.networkAddress = ip;
        manager.StartClient();
       

        yield return new WaitForSeconds(1.0f);

        ClientScene.Ready(manager.client.connection);

        if (ClientScene.localPlayers.Count == 0)
        {
            ClientScene.AddPlayer(0);
        }
    }
}
