﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Spawn : NetworkBehaviour
{
    [SerializeField]
    private GameObject spawnObject;

    private NetworkManager networkManager;
    // Use this for initialization
    void Start()
    {
        networkManager = FindObjectOfType<NetworkManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1") && networkManager.isNetworkActive )
        {
            
            GameObject obj = (GameObject)Instantiate(
                spawnObject, 
                networkManager.startPositions[0].position, 
                spawnObject.transform.rotation );

            NetworkServer.Spawn(obj);
        }
    }
}
