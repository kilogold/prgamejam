using UnityEngine;
using System.Collections;

public class NavigationTarget : MonoBehaviour
{
    private NavMeshAgent navAgent;

    public void AssignAgent(NavMeshAgent navAgentIn)
    {
        if (null == navAgent)
        {
            navAgent = navAgentIn;
        }
        else
        {
            Debug.LogError("NavmeshAgent already assigned. Ignoring.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        navAgent.SetDestination( transform.position);
    }
}
