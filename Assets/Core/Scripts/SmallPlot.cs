﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class SmallPlot : MonoBehaviour
{

	// Use this for initialization
    public Camera camera;
     void Start() 
    {
        camera = GameObject.Find("Camera").GetComponent<Camera>();
        IsPlotEmpty = true;
        IsMenuOpen = false;
        mesh = GetComponent<MeshRenderer>();
        if (mesh == null)
        {
            Debug.Log("null mesh");
        }
	}
    public GameObject FirstBuilding;
    public GameObject SecondBuilding;
    public GameObject PlotObject;
   
    public MeshRenderer mesh;
    public bool IsMenuOpen;
    public bool IsPlotEmpty;
    public  void ClearPlot()
    {
        Destroy(PlotObject);
        mesh.enabled = true;
        IsPlotEmpty = true;
    }
    void OnGUI()
    {
        // Bail out immediately if not moused over:
        if (!IsMenuOpen) return;
        // Get the screen position of the NPC's origin:
        Vector3 screenPos = camera.WorldToScreenPoint(transform.position);
        // Define a 100x100 pixel rect going up and to the right:
        Rect menuRect = new Rect(screenPos.x, screenPos.y, 50, 50);
        // Draw a label in the rect:
        GUI.Button(menuRect, "Hangar");
        menuRect = new Rect(screenPos.x - 100, screenPos.y, 50, 50);
        GUI.Button(menuRect, "Factory");
    }
	// Update is called once per frame
	protected  void Update () 
    {
	}
}
