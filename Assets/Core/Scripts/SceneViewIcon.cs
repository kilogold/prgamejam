using UnityEngine;
using System.Collections;

public class SceneViewIcon : MonoBehaviour
{
    public Transform target;
    public float radius;
    public Color color;
    public bool ShowDebugSpheres;

    public void OnDrawGizmos()
    {
        if (!target)
            return;
        if (!ShowDebugSpheres)
        {
            return;
        }
        Gizmos.color = color;
        Gizmos.DrawSphere(target.position, radius);
    }
}
