using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkCommanderOwnership : NetworkBehaviour
{
    [SyncVar(hook = "OnOwnershipChange")]
    public NetworkInstanceId commanderNetId;

    public NetworkCommander Commander;

    public event Action<NetworkCommander> NewOwnerEvent; 


    private void OnOwnershipChange(NetworkInstanceId newOwningCommanderNetId)
    {
        Debug.Log("NewOwner for " + gameObject.name);
        commanderNetId = newOwningCommanderNetId;
        Commander = ClientScene.objects[commanderNetId].GetComponent<NetworkCommander>();

        if (NewOwnerEvent != null)
        {
            NewOwnerEvent( Commander );
        }
    }
    public bool IsOwnByCommander(NetworkCommander commander)
    {
        if(null == commander)
        {
            Debug.LogError("Commander is null");
            return false;
        }
        return (this.commanderNetId == commander.netId);

    }
    public void AssignCommander(NetworkCommander commanderIn)
    {
        // This sets all clients to the proper value via syncvar
        commanderNetId = commanderIn.netId;
    }
}
