using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkCommander : NetworkBehaviour
{
    // Materials to identify teams
    public Material OwnershipMaterial;
    public Transform designatedCameraAnchor;

    [SerializeField]
    private GameObject commanderViewCameraPrefab;

    
    void Awake()
    {
        // All commanders should follow the name naming convention and should be placed in the same location.
        MyCamera.IsCameraEnabled = true;
        gameObject.transform.parent = GameObject.Find("Network").transform;
        gameObject.name = "NetworkCommander";        
    }
    
    public override void OnStartLocalPlayer()
    {
        // We have been notified that we are the local player.
        // Let's spawn a camera for the commander(us) to see what's going on.
        

        // We are operating client-side... However we need to have the server send us the 
        // designatedCameraAnchor before being in this method.
        // How do we do this?
        // I think we can do this by assigning the camera initialization AFTER the commander is online.
        // The NetworkManager will take care of it.

        // Assign an identifier to know which of all commanders is our local one.
        gameObject.name += "Local";
        
        // Give the commander (us) input via interface.
        gameObject.AddComponent<NetworkCommanderInterface>();
    }

    public void FlagCommanderAsVictor()
    {
        //This commander has been flagged as victor.
        //Let's notify all other commadners via RPC.
        Transform allCommanders = GameObject.Find("Network").transform;

        foreach (Transform curCommander in allCommanders)
        {
            curCommander.GetComponent<NetworkCommander>().RpcAnnounceVictor(netId);
        }

        GetComponent<NetworkCommander>().RpcAnnounceVictor(netId);

    }

    [ClientRpc]
    public void RpcKillDisableUnit( NetworkInstanceId unitNetId )
    {
        ClientScene.objects[unitNetId].gameObject.SetActive(false);
    }

    [ClientRpc]
    public void RpcAnnounceVictor(NetworkInstanceId commanderNetId)
    {
        bool didWin = (commanderNetId == netId);
        Cardboard.SDK.transform.GetComponent<MarroneoToaster>().PresentToast(didWin);
    }

    [ClientRpc]
    public void RpcInitializeCommanderCamera(string cameraAnchorGameObjectHierarchyName)
    {
        if (isLocalPlayer)
        {
            // The host will call this for itself, as well as other connecting commanders.
            // Although the 
            designatedCameraAnchor = GameObject.Find(cameraAnchorGameObjectHierarchyName).transform;
            GameObject commanderViewCameraInstance =
                Instantiate(commanderViewCameraPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            commanderViewCameraInstance.transform.parent = designatedCameraAnchor;
            commanderViewCameraInstance.transform.localPosition = Vector3.zero;
            commanderViewCameraInstance.transform.localRotation = Quaternion.identity;

            Transform cardboardHead = Cardboard.SDK.transform.FindChild("Head");
            NetworkCommanderInterface commanderInterface = GetComponent<NetworkCommanderInterface>();
            commanderInterface.cameraHead = cardboardHead;
        }
    }

    //HACK: This method is pretty hacky, but w/e...
    [ClientRpc]
    public void RpcInitializeCommanderOwnershipUnits()
    {
        //Server will call this method during player-connecting.
        //The receiver will follow up by searching for the corresponding factory it should own,
        // and it will command the server to register that factory to him/her.
        if (isLocalPlayer)
        {
            string camAnchorName = Cardboard.SDK.transform.parent.name;
            char correspondingFactorySuffix = camAnchorName[camAnchorName.Length - 1];
            string factoryGameObjectName = "TankFactory" + correspondingFactorySuffix;

            GameObject factoryGO = GameObject.Find(factoryGameObjectName);
            CmdAssignCommanderToFactory( factoryGO );
        }
    }

    [Command]
    public void CmdSpawnObjectFromFactory( GameObject factory )
    {
        // Need to declare method using GameObject instead of Component, because UNET 
        // doesn't like that. Compile error.
        factory.GetComponent<UnitFactory>().SpawnUnitForCommander();
    }

    [Command]
    public void CmdAssignCommanderToFactory( GameObject factory )
    {
        factory.GetComponent<UnitFactory>().AssignCommander(this);
    }

    [Command]
    public void CmdMoveUnit(GameObject unit, Vector3 target)
    {
        unit.GetComponent<UnitCommands>().Move(target);
    }
}
