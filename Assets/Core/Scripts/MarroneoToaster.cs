using UnityEngine;
using System.Collections;

public class MarroneoToaster : MonoBehaviour
{
    public void PresentToast(bool didWin)
    {
        if (didWin)
        {
            Application.LoadLevel("Win");
        }
        else
        {
            Application.LoadLevel("Lose");
        }
    }
}
