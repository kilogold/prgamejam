using System;
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class UnitStatsBehavior : NetworkBehaviour
{
    [SerializeField]
    public UnitStats baseStats;

    public int currentHealth;
    public int unitSpeed;
    public int unitRotationSpeed;
    public NetworkCommanderOwnership CommanderOwnership { get; private set; }
    void Awake()
    {
        currentHealth = baseStats.maxHealth;
        CommanderOwnership = GetComponent<NetworkCommanderOwnership>();
        unitSpeed = baseStats.movementSpeed;
        unitRotationSpeed = baseStats.rotationSpeed;
    }

    /// <summary>
    /// Set the color of the unit based on commander.
    /// </summary>
    [ClientRpc]
    private void RpcSetTeamColor(Color commanderMaterialColor)
    {
        // Assign the commander color
        MeshRenderer[] meshRenderers = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer rend in meshRenderers)
        {
            rend.material.color = commanderMaterialColor;
        }
    }

    public void ApplyDamage( int damage)
    {
        currentHealth -= damage;
        if(currentHealth <= 0)
        {
            CommanderOwnership.Commander.RpcKillDisableUnit(netId);
            //this.gameObject.SetActive(false);
            Debug.Log("unit destroy");
        }
    }
    public void AssignCommander(NetworkCommander commanderIn)
    {
        CommanderOwnership.AssignCommander(commanderIn);
        RpcSetTeamColor(CommanderOwnership.Commander.OwnershipMaterial.color);
    }
   
}
