using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class CustomNetworkManager : NetworkManager
{
    public Material[] PlayerMaterials;
    public Transform[] PlayerCamerasAnchors;

    //HACK:
    // We should have some way to get this info from the networking api.
    // Can't figure out how...
    private int currentConnectedCommanders = 0;

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        ///////////////////////////////
        /// Instantiate core instance
        ///////////////////////////////
        GameObject playerObject = (GameObject)Instantiate(playerPrefab, playerPrefab.transform.position, Quaternion.identity);
        NetworkCommander commander = playerObject.GetComponent<NetworkCommander>();

        ////////////////////
        //  Extra assembly
        /////////////////////
        Debug.Log("Connecting Commander assigned with material index [" + currentConnectedCommanders + "]");
        commander.OwnershipMaterial = PlayerMaterials[currentConnectedCommanders];


        /////////////////////////////////
        /// Submit assembled commander
        /////////////////////////////////
        NetworkServer.AddPlayerForConnection(conn, playerObject, playerControllerId);

        //////////////////////////////////////////////////
        /// Finish configuring the now-online commander
        //////////////////////////////////////////////////
        Debug.Log("Connecting Commander assigned with camera anchor " + PlayerCamerasAnchors[currentConnectedCommanders].name);
        commander.RpcInitializeCommanderCamera( PlayerCamerasAnchors[currentConnectedCommanders].gameObject.name );

        Debug.Log("Connecting Commander assigned ownership of factory");
        commander.RpcInitializeCommanderOwnershipUnits();

        currentConnectedCommanders++;

    }
}
