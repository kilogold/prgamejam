﻿using UnityEngine;
using System.Collections;

public class AggroSphereCollision : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        Debug.Log("Ontrigger start");
    }

    void OnTriggerStay()
    {

    }
    void OnTriggerExit()
    {
    }
    void OnTriggerEnter(Collider collidingObject)
    {
       // Debug.Log("Ontrigger enter");
        Collider collider = collidingObject.GetComponent<Collider>();
        if (collider.tag == "AggroSphere" )
        {
            //let's find out our parent
            AiStateMachine stateMachine = GetComponentInParent<AiStateMachine>();
            
            //Debug.Log("inside sphere collides");
            if (null == stateMachine)
            {
                Debug.LogError("Collided with a aggrosphere where the parent has no statemachine.");
                return;
            }
            
            UnitStatsBehavior myStatsBehavior=   GetComponentInParent<UnitStatsBehavior>();
            if(null ==myStatsBehavior )
            {
                Debug.LogError("unit stats behavior not foudn on aggrosphere collision");
                return;
            }
            UnitStatsBehavior otherStatsBehavior = collidingObject.GetComponentInParent<UnitStatsBehavior>();
            if (null == otherStatsBehavior)
            {
                Debug.LogError("unit stats behavior not foudn on aggrosphere colliding objects");
                return;
            }
            //dont run on friendly players
            if(myStatsBehavior.CommanderOwnership.IsOwnByCommander(otherStatsBehavior.CommanderOwnership.Commander))
            {
                return;
            }
            AiState_TankFiring tank_firing_state = GetComponentInParent<AiState_TankFiring>();

            tank_firing_state.TargetUnit = collidingObject.gameObject;
            stateMachine.SetState(tank_firing_state);

            return;
        }
        else if(collider.tag == "Building")
        {
            //let's find out our parent
            AiStateMachine stateMachine = GetComponentInParent<AiStateMachine>();
            
            //Debug.Log("inside sphere collides");
            if (null == stateMachine)
            {
                Debug.LogError("Collided with a aggrosphere where the parent has no statemachine.");
                return;
            }
            
            UnitStatsBehavior myStatsBehavior=   GetComponentInParent<UnitStatsBehavior>();
            if(null ==myStatsBehavior )
            {
                Debug.LogError("unit stats behavior not foudn on aggrosphere collision");
                return;
            }
            UnitFactory otherStatsBehavior = collidingObject.GetComponentInParent<UnitFactory>();
            if (null == otherStatsBehavior)
            {
                Debug.LogError("unit stats behavior not foudn on aggrosphere colliding objects");
                return;
            }
            //dont run on friendly players
            if(otherStatsBehavior != null && myStatsBehavior.CommanderOwnership.IsOwnByCommander(otherStatsBehavior.CommanderOwnership.Commander))
            {
                return;
            }
            AiState_TankFiring tank_firing_state = GetComponentInParent<AiState_TankFiring>();

            tank_firing_state.TargetUnit = collidingObject.gameObject;
            stateMachine.SetState(tank_firing_state);

            return;
        }
        //
    }
}
