﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class NetworkMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
        manager = GameObject.FindObjectOfType<NetworkManager>();
        IsStarted = false;
	}
    public string ip;
    public NetworkManager manager;
    public bool IsStarted;
	// Update is called once per frame
	void OnGUI ()
    {
        if(IsStarted)
        {
            return;
        }
        if(null == manager)
        {
            Debug.LogError("network manager null on gui");
        }
        if (GUI.Button(new Rect(10, 10, 120, 30), "Host Server"))
        {
            manager.StartHost(); 
            
        }
        ip = GUI.TextField(new Rect(200, 50, 100, 30), "localhost");
        if (GUI.Button(new Rect(10, 50, 120, 30), "Join Server"))
        {
            
            manager.networkAddress = ip;
            manager.StartClient();
            
        }

        ClientScene.Ready(manager.client.connection);

        if (ClientScene.localPlayers.Count == 0)
        {
            ClientScene.AddPlayer(0);
        }
        IsStarted = true;
	}
}
