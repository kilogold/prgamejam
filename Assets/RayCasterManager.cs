﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class RayCasterManager : MonoBehaviour
{
    public GameObject selectedUnit;
    public Camera camera;
    void Start()
    {
        camera = GameObject.Find("Camera").GetComponent<Camera>();
    }
    private GameObject PlotObject;
   
    void Update()
    {
        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        //we hit something
        if (Physics.Raycast(ray, out hit))
        {
            //is it a building??
            Component buildings = hit.collider.gameObject.GetComponent("Building");
            if(buildings != null)
            {
                if(Input.GetKey(KeyCode.C))
                {
                    Building asd = (Building)buildings;
                    asd.ParentPlot.ClearPlot();
                }
            }
            //is it a plot of land??
            Component asdasd = hit.collider.gameObject.GetComponent("SmallPlot");
            if ( asdasd != null)
            {
                //you hitted a small plot check the key
                SmallPlot plot = (SmallPlot)asdasd;
                if (Input.GetKey(KeyCode.C))
                {
                    plot.ClearPlot();
                }
                if (Input.GetKey(KeyCode.Space))
                {
                    plot.IsMenuOpen = true;
                }
                if (plot.IsMenuOpen)
                {
                    //TODO:change this  to get the distance from the middle.
                    if (Input.GetKey(KeyCode.LeftArrow))
                    {
                        //at
                        PlotObject = (GameObject)Instantiate(plot.FirstBuilding, hit.collider.transform.position, hit.collider.transform.rotation);
                        PlotObject.GetComponent<Building>().ParentPlot = plot;
                        plot.PlotObject = PlotObject;
                        plot.IsPlotEmpty = false;
                        plot.mesh.enabled = false;
                        plot.IsMenuOpen = false;
                    }
                    else if (Input.GetKey(KeyCode.RightArrow))
                    {
                        //aa
                        PlotObject = (GameObject)Instantiate(plot.SecondBuilding, hit.collider.transform.position, hit.collider.transform.rotation);
                        PlotObject.GetComponent<Building>().ParentPlot = plot;
                        plot.PlotObject = PlotObject;
                        plot.IsPlotEmpty = false;
                        plot.mesh.enabled = false;
                        plot.IsMenuOpen = false;
                    }
                }
            }
        }
        
    }
}
